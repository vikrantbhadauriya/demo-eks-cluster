
variable "bucket_name" {
  type    = string
  default = "test-dev-eks"
}

variable "acl_value" {
    default = "private"
}

variable "aws_region" {
  type    = string
  default = "eu-central-1"
}