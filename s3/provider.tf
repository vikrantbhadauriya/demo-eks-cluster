provider "aws" {
  region  = var.aws_region
  profile = "aws.CP.CorporateIT.Dev"
}



terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.73.0"
    }
  }
}