provider "aws" {
  region  = var.region
  profile = "aws.CP.CorporateIT.Dev"
}

terraform {
  backend "s3" {
    encrypt        = true
    bucket         = "test-dev-eks"
    key            = "tfstate/test-dev-eks/test-eks.tfstate"
    region         = "eu-central-1"
    profile        = "aws.CP.CorporateIT.Dev"
    # dynamodb_table = "test-dev-dynamodb-state-locking"
  }
}

terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=5.0.0"
    }
  }
}

data "aws_availability_zones" "available" {}