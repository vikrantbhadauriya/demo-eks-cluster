


module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.5.1"

  cluster_name    ="${var.project}-${var.environment}"
  cluster_version =   var.cluster_version

  cluster_endpoint_public_access = true

  cluster_addons = {
    
    coredns = {
      resolve_conflicts = "OVERWRITE"
     
      most_recent = true
    }
    kube-proxy = {
      resolve_conflicts = "OVERWRITE"
   
      most_recent = true
    }
    vpc-cni = {
      enabled = true
   
      resolve_conflicts = "OVERWRITE"
      most_recent = true
    }
  }
  vpc_id                   = "vpc-8ef244e4"
  subnet_ids               = ["subnet-b647d9dc", "subnet-392cf945", "subnet-8b4991c7"]

  eks_managed_node_group_defaults = {
    instance_types = ["m6i.large", "m5.large", "m5n.large", "m5zn.large"]
  }
  eks_managed_node_groups = {
   
    one = {
       enable_monitoring            = false
      name = "node-group-1"

      instance_types = ["t3.small"]

      min_size     = 1
      max_size     = 3
      desired_size = 1
    }

    green = {
       enable_monitoring            = false
      name = "node-group-2"

      instance_types = ["t3.small"]

      min_size     = 1
      max_size     = 2
      desired_size = 1
    }
  }
  tags = {
    Environment = "dev-test-demo"
    Terraform   = "true"
  }
}