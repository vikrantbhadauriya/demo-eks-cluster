# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

variable "region" {
  description = "AWS region"
  type        = string
  default     = "eu-central-1"

}

variable "environment" {
  type    = string
  default = "dev-test"
}

variable "project" {
  type    = string
  default = "demo-vikrant"
}

variable "cluster_version" {
  type    = string
  default = "1.27"
}